package kata;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class TestPrimeFactor {

    private ArrayList expect;

	@Before
    public void setUp() {
        expect = new ArrayList<Integer>();
    }

    @Test
    public void oneShouldReturnEmpty() {
        assertEquals("not eq", expect, PrimeFactor.getFactor(1));
    }

    @Test
    public void twoShouldReturnTwo() {
        expect.add(2);
        assertEquals("not eq", expect, PrimeFactor.getFactor(2));
    }

    @Test
    public void fourShouldReturnTwoTwo() {
        expect.add(2);
        expect.add(2);
        assertEquals("not eq", expect, PrimeFactor.getFactor(4));
    }

    @Test
    public void threeShouldReturnThree() {
        expect.add(3);
        assertEquals("not eq", expect, PrimeFactor.getFactor(3));
    }

    @Test
    public void eightShouldReturnTwoTwoTwo() {
        expect.add(2);
        expect.add(2);
        expect.add(2);
        assertEquals("not eq", expect, PrimeFactor.getFactor(8));
    }
} 
package kata;

import java.util.ArrayList;
import java.util.List;

class PrimeFactor{
	public static ArrayList<Integer> getFactor(int number){
        ArrayList result = new ArrayList<Integer>();
        int factor = number;

        if (number == 1)
            return result;

        while (factor > 1){
            if (factor % 2 == 0){
                result.add(2);
            }

            factor /= 2;
        }

        if (result.size() == 0)
            result.add(number);

        return result;
    }
}